		<div class="form_addNewEntry">
			<form class="form-horizontal" role="form" action="/" method="post">
			  	<div class="form-group">
				    <label for="dateInspection" class="col-sm-3 control-label">Дата осмотра</label>
				    <div class="col-sm-3">
				      	<input type="text" class="form-control" id="dateInspection" placeholder="<?print $dateToday;?>" value="<?print $dateToday;?>" name="dateInspection">
				    </div>

				    <label for="dateRepair" class="col-sm-3 control-label">Дата ремонта</label>
				    <div class="col-sm-3" id="picker-container">
				      	<input type="text" class="form-control datepicker" placeholder="<?print $dateToday;?>" name="dateRepair">
				    </div>
			  	</div>
			  	<div class="form-group">
				    <label for="repairPeriod" class="col-sm-3 control-label">Срок ремонта</label>
				    <div class="col-sm-9">
				      	<input type="text" class="form-control" id="repairPeriod" placeholder="3" name="repairPeriod">
				    </div>
			  	</div>
				<?if(!empty($channelSale)){?>
			  	<div class="form-group">
				    <label for="howKnow" class="col-sm-3 control-label">Как узнали</label>
				    <div class="col-sm-9">
				    	<select class="form-control">
				    		<option value="0">---</option>
				    	<?foreach ($channelSale as $key => $value) {
				    		print '<option value="'.$key.'">'.$value.'</option>';
				    	}?>
				    </select>
				    </div>
			  	</div>
				<?}?>
			  	<div class="form-group">
				    <label for="phone" class="col-sm-3 control-label">Телефон</label>
				    <div class="col-sm-9">
				      	<input type="text" class="form-control" id="phone" placeholder="+7XXXXXXXXXX" name="phone">
				    </div>
			  	</div>
			  	<div class="form-group">
				    <label for="name" class="col-sm-3 control-label">Имя</label>
				    <div class="col-sm-9">
				      	<input type="text" class="form-control" id="name" placeholder="Иванов Иван Иванович" name="name">
				    </div>
			  	</div>
			  	<div class="form-group">
				    <label for="car" class="col-sm-3 control-label">Автомобиль</label>
				    <div class="col-sm-9">
				      	<input type="text" class="form-control" id="car" placeholder="Toyota Land Cruiser Prado" name="car">
				    </div>
			  	</div>
			  	<div class="form-group">
				    <label for="carNumber" class="col-sm-3 control-label">Госномер</label>
				    <div class="col-sm-9">
				      	<input type="text" class="form-control" id="carNumber" placeholder="X999XX134" name="carNumber">
				    </div>
			  	</div>
			  	<div class="form-group">
				    <label for="whoInspection" class="col-sm-3 control-label">Кто делал осмотр</label>
				    <div class="col-sm-9">
				      	<input type="text" class="form-control" id="whoInspection" placeholder="Роман Бондаренко" name="whoInspection">
				    </div>
			  	</div>

			  	<?if(!empty($typeRepair)){?>
			  	<div class="form-group">
				    <label for="typeRepair" class="col-sm-3 control-label">Вид ремонта</label>
				    <div class="col-sm-9">
				    	<?foreach ($typeRepair as $key => $value) {
				    		print '<input type="checkbox" name="typeRepair[]" value="'.$key.'"> '.$value.' ';
				    	}?>
				    </div>
			  	</div>
			  	<?}?>

				<div class="listDamage">
				  	<h5>Повреждения</h5>

					<div class="form-group elementDamageList">
					    <div class="col-sm-3">
					      	<select  class="form-control" name="calculate_cost">
					      		<option>Код работы</option>
				      			<?foreach ($listTypeRepair as $type) {
				      				print '<option value="'.$type['name'].'" data-hours="'.$type['hours'].'" data-mater="'.$type['materials'].'">'.$type['name'].'</option>';
				      			}?>
					      	</select>
					    </div>
					    <div class="col-sm-3">
					      	<select class="form-control">
					      		<option>Деталь</option>
				      			<?foreach ($listPartRepair as $part) {
				      				print '<option>'.$part['name'].'</option>';
				      			}?>
					      	</select>
					    </div>
					    
				  	</div>

				  		<div class="col-sm-6 addButton">
				      		<button type="button" id="buttonAdd" class="btn btn-default">Добавить</button>
				    	</div>
				</div>

			  	<div class="form-group">
				    <div class="col-sm-3">
				      	<p>Сумма: 2100р</p>
				    </div>
			  	</div>

			  	<div class="form-group">
				    <div class="col-sm-10">
				      	<button type="submit" name="saveBtn" value="saveBtn" class="btn btn-success">Сохранить</button>
				    </div>
			  	</div>
			</form>
		</div>