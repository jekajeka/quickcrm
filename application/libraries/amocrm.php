<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Amocrm {

  public function __construct(){

  }

  function amo_auth(){

  $USER_LOGIN = 'm.kupriyanova@monodigital.ru';
  $USER_HASH = '857dbd98c5e10b30d999ce03ec18b430';
  $POST_URL = 'https://quickcrm.amocrm.ru/private/api/auth.php?type=json';

    $user=array(
      'USER_LOGIN'=>$USER_LOGIN,
      'USER_HASH'=>$USER_HASH
    );

    $curl=curl_init();
    curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
    curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
    curl_setopt($curl,CURLOPT_URL,$POST_URL);
    curl_setopt($curl,CURLOPT_CUSTOMREQUEST,'POST');
    curl_setopt($curl,CURLOPT_POSTFIELDS,json_encode($user));
    curl_setopt($curl,CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
    curl_setopt($curl,CURLOPT_HEADER,false);
    curl_setopt($curl,CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookie.txt');
    curl_setopt($curl,CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookie.txt');
    curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
    curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);
     
    $out=curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную
    $code=curl_getinfo($curl,CURLINFO_HTTP_CODE); #Получим HTTP-код ответа сервера
    curl_close($curl); #Завершаем сеанс cURL

    $code=(int)$code;
    $errors=array(
      301=>'Moved permanently',
      400=>'Bad request',
      401=>'Unauthorized',
      403=>'Forbidden',
      404=>'Not found',
      500=>'Internal server error',
      502=>'Bad gateway',
      503=>'Service unavailable'
    );
    try
    {
      #Если код ответа не равен 200 или 204 - возвращаем сообщение об ошибке
      if($code!=200 && $code!=204)
        throw new Exception(isset($errors[$code]) ? $errors[$code] : 'Undescribed error',$code);
    }
    catch(Exception $E)
    {
      die('Ошибка: '.$E->getMessage().PHP_EOL.'Код ошибки: '.$E->getCode());
    }
     
    /**
     * Данные получаем в формате JSON, поэтому, для получения читаемых данных,
     * нам придётся перевести ответ в формат, понятный PHP
     */
    // $Response=json_decode($out,true);
    // $Response=$Response['response'];
    // if(isset($Response['auth'])) #Флаг авторизации доступен в свойстве "auth"
    //   return 'Авторизация прошла успешно';
    // return 'Авторизация не удалась';

  }

  function get_customFields(){
    $link = 'https://quickcrm.amocrm.ru/private/api/v2/json/accounts/current';
    $list = $this->add_data($link, '');

    $list=$list['response']['account']['custom_fields'];
    return $list;
  }

  function add_lead($contacts){
    $link = 'https://quickcrm.amocrm.ru/private/api/v2/json/leads/set';
    $id_leed = $this->add_data($link, $contacts);

    $id_leed=$id_leed['response']['leads']['add'];
     
    $output=array();
    foreach($id_leed as $v)
      if(is_array($v))
        $output[]=$v['id'];
    $output = implode(',', $output);

    return $output;
  }

  function add_company($company){
    $link = 'https://quickcrm.amocrm.ru/private/api/v2/json/company/set';
    $this->add_data($link, $company);
  } 

  function add_contact($contact){
    $link = 'https://quickcrm.amocrm.ru/private/api/v2/json/contacts/set';
    $this->add_data($link, $contact);
  }  

  function add_data($link, $data){
    $curl=curl_init();
    curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
    curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
    curl_setopt($curl,CURLOPT_URL,$link);
    if(!empty($data)){
      curl_setopt($curl,CURLOPT_CUSTOMREQUEST,'POST');
      curl_setopt($curl,CURLOPT_POSTFIELDS,json_encode($data));
      curl_setopt($curl,CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
    }
    curl_setopt($curl,CURLOPT_HEADER,false);
    curl_setopt($curl,CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
    curl_setopt($curl,CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
    curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
    curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);
     
    $out=curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную
    $Response=json_decode($out,true);
    return $Response;

  }
}