<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Quickcrm extends CI_Controller{

	function __construct(){
        parent::__construct();
    }

	function index(){
		$this->data['code_page'] = 'mainpage';
		$this->load->model('inspection');
		
		$listTypeRepair = $this->inspection->getListTypeRepair();
		$this->data['listTypeRepair'] = $listTypeRepair;
		$listPartRepair = $this->inspection->getListPartRepair();
		$this->data['listPartRepair'] = $listPartRepair;

		$this->load->library('amocrm');
		$this->amocrm->amo_auth();

		$list_custom_fields = $this->amocrm->get_customFields();
		$lead_fields = $list_custom_fields['leads'];
		$contact_fields = $list_custom_fields['contacts'];

		foreach ($lead_fields as $key) {
			if($key['name'] == 'Вид ремонта'){
				$this->data['typeRepair'] = $key['enums'];
			}

			if($key['name'] == 'Канал продаж'){
				$this->data['channelSale'] = $key['enums'];
			}
		}

		// echo '<pre>';
		// var_dump($list_custom_fields['contacts']);
		// echo '</pre>';

		$saveBtn = $this->input->post('saveBtn');
		if($saveBtn){
			$dateinsp      = $this->input->post('dateInspection');
			$dateRepair    = $this->input->post('dateRepair');
			$repairPeriod  = $this->input->post('repairPeriod');
			$howKnow 	   = $this->input->post('howKnow');
			$phone         = $this->input->post('phone');
			$name 		   = $this->input->post('name');
			$car 		   = $this->input->post('car');
			$carNumber 	   = $this->input->post('carNumber');
			$whoInspection = $this->input->post('whoInspection');
			$typeRepair    = $this->input->post('typeRepair');
			// $typeRepair    = implode(',',$typeRepair);

			// echo '<pre>';
			// var_dump($typeRepair);
			// echo '</pre>';

				//разобраться почему все так сложно!!!!

			$date_inspect_custom_fields = array();
			$date_repair_custom_fields = array();
			$period_repair_custom_fields = array();
			$type_repair_custom_fields = array();

			foreach ($lead_fields as $key) {
				if($key['name'] == 'Дата осмотра')
					$date_inspect_custom_fields = array(
							'id' => $key['id'],
							'values' => array(array('value' => $dateinsp))
						);

				if($key['name'] == 'Дата ремонта')
					$date_repair_custom_fields = array(
							'id' => $key['id'],
							'values' => array(array('value' => $dateRepair))
						);

				if($key['name'] == 'Срок ремонта')
					$period_repair_custom_fields = array(
							'id' => $key['id'],
							'values' => array(array('value' => $repairPeriod))
						);

				if($key['name'] == 'Вид ремонта')
					$type_repair_custom_fields = array(
							'id' => $key['id'],
							'values' => array($typeRepair)
						);
			};

			$phone_custom_fields = array();

			foreach ($contact_fields as $key) {
				if($key['name'] == 'Телефон')
					$phone_custom_fields = array(
							'id' => $key['id'],
							'values' => array(array('value' => $phone,
													'enum'=>'MOB'))
						);
			};

			$leads['request']['leads']['add']=array(
				array(
				    'name' => $car.' | '.$carNumber,
				    'custom_fields' => array($date_inspect_custom_fields, 
									    	$date_repair_custom_fields,
									    	$period_repair_custom_fields,
									    	$type_repair_custom_fields)
				)
			);
			$id_leed = $this->amocrm->add_lead($leads);

			$company['request']['contacts']['add']=array(
			  	array(
			    'name'=>$car.' | '.$carNumber,
			    'linked_leads_id'=>array($id_leed)
			    )
			);
			$this->amocrm->add_company($company);

			$contacts['request']['contacts']['add']=array(
			  	array(
				    'name'=>$name,
				    'linked_leads_id'=>array($id_leed),
				  	'company_name'=>$car.' | '.$carNumber,
				  	'custom_fields' => array($phone_custom_fields)
			    )
			 );

			$this->amocrm->add_contact($contacts);

		}

		$this->data['dateToday'] = date('d.m.Y');
		$this->dataloc['content'] = $this->load->view('inspectionForm', $this->data,true);
		$this->load->view('main', $this->dataloc);
	}

}



