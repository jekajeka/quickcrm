<?php
class Inspection extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function getListTypeRepair(){
    	$sql = "select name, hours,materials from repair_types";
    	$query = $this->db->query($sql);
    	return $query->result_array();
    }
    function getListPartRepair(){
    	$sql = "select name from repair_parts";
    	$query = $this->db->query($sql);
    	return $query->result_array();
    }
}
?>