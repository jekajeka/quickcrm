	  	$(function () {
		    $('.datepicker').datepicker({
		    	language: 'ru',
		    	format: 'dd.mm.yyyy',
		    	todayHighlight: 'true',
		    	autoclose:true
		    });

		    $("#phone").mask("+79999999999");

		    $(".elementDamageList:first").clone(true).appendTo(".listDamage");

		    $("#buttonAdd").on('click', function(){

		    	var el = $('.elementDamageList:first').clone(true);
				$(el).appendTo('.listDamage');

				$(".elementDamageList:last").append($(".addButton"));									    
		    });

		    $("select[name=calculate_cost]").on('change', function(){
		    	var data = $('option:selected').attr('data-hours');
		    	console.log(data);
		    })

	  	});